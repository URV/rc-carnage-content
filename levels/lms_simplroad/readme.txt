#################################################################################################
##################### 	              Road of Simplicity LMS    	    #####################
#####################    "Last Man Standing" game mode arena for Re-Volt    #####################
#####################	 		   By VaiDuX461    		    #####################
#################################################################################################
Check my files: (www.mediafire.com/?dbg9nyxxnwc16) for updates, fixes and more stuff.
(Recommended to view this at 800x600 or higher resolution, if you can't, please buy a new PC)

========================================= Information ===========================================

Name: Road of Simplicity LMS
Release date: 2013-05-05
Folder name: lms_simplroad
Track Type: Last Man Standing (Online only)
Difficulty: Extreme
Number of Stars: 2
Custom Features: Yes
Bonus Features: No

Recommended to use latest Re-Volt v1.2 patch.


========================================== Description ==========================================

It's arena for "Last Man Standing" game mode, based on "Road of Simplicity 1" track.

In "Last Man Standing" game mode, you must knock out all players with pick-ups from platforms.
Last surviror on plaform wins. Be carefull, don't fall off!

Rules:
 � Don't fall off the platform
 � Get the other player off the platform with pick-ups


======================================== How to install =========================================

Just extract 2 folders (levels, gfx) to main Re-Volt directory,
default: (C:\Program Files\Acclaim Entertainment\Re-Volt).

If you don't like custom features (some people should): custom sounds and graphics, you can 
delete or rename folder "custom" inside "\levels\lms_simplroad", but I don't recommend doing that,
there's special (darker) font to make easier to read, so without it you will hardly see symbols.
Anyway, you can still delete sounds if they annoys you for some reason.


======================================= How to uninstall ========================================

Delete "lms_simplroad.bmp" file from "gfx" folder and "lms_simplroad" from "levels".
Or download WolfR4 (http://jigebren.free.fr/games/pc/re-volt/wolfr4/) and delete them instantly.
I think there are other track management tools.


========================================== Known Bugs ===========================================

- Some line holes in ground (they don't do bad, you can just see them)

Sorry, I can't fix it ^^. If you seen other bugs, please let me know: "vaidux461@gmail.com" or
PM me in "Re-Volt Live" and "Our Re-Volt Pub" forums.


===================================== Used Tools and Media ======================================

MAKEITGOOD Editors, Wall prm kit, rvglue, prm GUI, prm2ncp, ms notepad, various sounds from old games:
(1943, F1 Race, Mega Man 3, Mega Man 4, Mario Bros, Super Mario Bros., Super Mario Bros 2,
Super Mario Bros 3, Pong).


============================================ Trivia =============================================

- My first arena for not very known game mode - "Last Man Standing".
- Based on original "Road of Simplicity 1" track idea.


=========================================== Thanks to ===========================================

Acclaim - You should know why.
Sh*t computer - I somehow made this.
Authors of rvtmod - for rvglue.
Capcom, Nintendo, Atari - for games and their sounds, which I stol... used in this track.


========================================= Re-Volt links =========================================

http://rv12.zackattackgames.com - Re-Volt V1.2 patch home page.
http://rvzt.zackattackgames.com/main/ - Re-Volt Zone Tracks, RVZT for short. You can download
 TONS of tracks and ALSO cars there.
http://z3.invisionfree.com/Our_ReVolt_Pub - Our ReVolt Pub or ORP forum.
http://z3.invisionfree.com/Revolt_Live - Revolt Live or RVL forum.
http://www.google.com - Really good page to find a full version of Re-volt.


========================================== Permissions ==========================================

You can distribute this thing, as long as you include this README FILE WITHOUT MODIFICATIONS.
Archive can be distribute in any electronic format: CD, DVD, HD DVD, Blu-ray, HVD, Laserdisc,
HDD, Floppy disk, USB flash drive, Compact Cassette, Memory card (CompactFlash, SmartMedia,
Memory Stick, Memory Stick Duo, Memory Stick PRO Duo, Memory Stick Micro M2, Multimedia Card, SD
miniSD, microSD, xD-Picture Card, etc), Social networks, Skype, File hosting sites, etc.
You're free to edit and distribute it, but DON'T EDIT MY README FILE and also MENTION ME IN YOUR
CREDITS, README OR WHATEVER. IF YOU WILL IGNORE THESE RULES, YOU WILL HAVE a BAD TIME.


#################################################################################################
Sorry for grammar errors
Have fun!
�2013, VaiDuX461
Track ver v0.8