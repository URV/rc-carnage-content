In preparation for All's Fair III, I started making some LMS arenas.
Either Saffron or Skarma came up with the idea to turn Chinatown into an LMS track.
I polished up some props from the track and put them into an arena.

The battle platform consists of three surface types: Gravel, ice 2 and carpet shag.
I think the setup I chose will make for some interesting battles.
There also are two platforms for spectating.

Due to the many lanterns, this track comes very close to the object limit of RV.

You need RVGL to play this: http://rv12.revoltzone.net

The music is Kaleidoscope Eyes by Overdream (shortened).

-Marv, 17.1005
re-volt.io