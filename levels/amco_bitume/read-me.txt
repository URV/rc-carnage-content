
***************
* AMCO Bitume *
***************



Credits :

Omessmer
Totolens
Smag
SebR
Clamour
Scorp1o
kao98
Pigasque
Manmountain
Killer Wheels

And special thanks to :

Aylown
Sjampo
Harry Plotter
cd090580
JSgdfl
Gustry
Sonic_000



To install Unzip into your main revolt folder.


**********
*FRANCAIS*
**********

Tous ceux qui ont particip� � la cr�ation de ce circuit vous remercient d'avoir t�l�charg�
la derni�re version (25/01/2009) du circuit AMCO Bitume !

********

Merci � tous ceux qui ont et qui continuent de jouer, de cr�er et d'aider dans Re-Volt.
Sans eux les tonnes d'heures de plaisir ne seraient pas l�, merci encore.

********

Bon jeu ! :)



*********
*ENGLISH*
*********

Every people who have helped to build this track thank you for downloading
the last release (01/25/2009) of the AMCO Bitume track !

********

Thanks to all who played and who are still playing, for its creation and help to Re-volt.
Without them, hours of pleasure playing this game wouldn�t exist, thanks again.

********

Enjoy :)


********
********

Pour connaitre toute l'histoire de AMCO Bitume, rendez-vous sur :
	http://www.rvtt.com/phpbb_fr/viewtopic.php?f=4&t=725

Le petit point feedback : allez voir sur http://www.nawakiwi.com/

(: Kiwi :)

***********************
Copyright / Permissions
***********************

This level is copyright 2009, by Kiwi.

You can't use this level as a base to build additional levels, if you wish to do so please
visit http://www.nawakiwi.com/ and ask on the forum

You are not allowed to commercially exploit this level, i.e. put it on a CD or any 
other electronic medium that is sold for money without my explicit permission!

You may distribute this level through any electronic network (internet (web/ftp), 
FIDO, local BBS etc.), provided you include this file and leave the archive intact.