Information
-----------------
Track Name:	Sideways Sanctuary D
Length:		287 meters
Difficulty	Hard
Author:		Saffron


Description
-----------------
A quick racetrack made with a bezier curve, and slightly extended with some eyecandy. It was inspired by the many tight circuits used for online drifting, and designed purely with that in mind.


Requirements
-----------------
You MUST use the latest RVGL patch for the road to display properly.


Credits
-----------------
The Internet, the RVTT team, Lo Scassatore and Polyphony Digital for the textures
Lo Scassatore for the original idea
The Blender Foundation for Blender
Marv for his Blender plug-in
Huki and Marv (again) for RVGL
Rick Brewster for Paint.NET
Victor for testing out of the track and giving feedback
Everyone in Re-Volt Discord