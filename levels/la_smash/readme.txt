======================================== Sept 2006
Track name: L.A.Smash  
Author: RickyD & hilaire9
Track length: 314 meters                                      
Type: Extreme
=======================================
*** Track Installation:  Extract into your main Re-Volt folder
----------------------------------------------------------------
--- Description:  Race around Los Angeles flood control channel.                  
----------------------------------------------------------------------         
* Tools: 3ds Max (RickyD), MAKEITGOOD edit modes (RickyD & hilaire9),
         rvGlue (hilaire9).  Several prms from Jimk and Revolt.
                      
--------- hilaire9's Home Page: http://members.cox.net/alanzia
|||=================================================================|||