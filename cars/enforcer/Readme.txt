Car information
================================================================
Car name                : Enforcer
Car Type  		: Original
Top speed 		: 36 MPH
Rating/Class   		: Advanced
Description             : The car that stops at your house when you post [REDACTED]

Author Information
================================================================
Author Name 		: flyboy
 
Construction
================================================================
Editor(s) used 		: Gimp, Blender
 
Copyright / Permissions
================================================================
Feel free to use the psd file to retexture the car.
 
