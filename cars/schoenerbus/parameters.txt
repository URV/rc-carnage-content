{

;============================================================================
;============================================================================
; What a nice bus! That's actually a truck. No, I'm pretty sure it's a train.
;============================================================================
;============================================================================
Name      	"Schönerbus"


;====================
; Model Filenames
;====================

MODEL 	0 	"cars\schoenerbus\karosserie.prm"
MODEL 	1 	"cars\schoenerbus\radlinks.prm"
MODEL 	2 	"cars\schoenerbus\radrechts.prm"
MODEL 	3 	"NONE"
MODEL 	4 	"NONE"
MODEL 	5 	"NONE"
MODEL 	6 	"NONE"
MODEL 	7 	"NONE"
MODEL 	8 	"NONE"
MODEL 	9 	"NONE"
MODEL 	10 	"NONE"
MODEL 	11 	"NONE"
MODEL 	12 	"NONE"
MODEL 	13 	"NONE"
MODEL 	14 	"NONE"
MODEL 	15 	"NONE"
MODEL 	16 	"NONE"
MODEL 	17 	"cars\misc\aerial.m"
MODEL 	18 	"cars\misc\aerialt.m"
TPAGE 	"cars\schoenerbus\bus.bmp"
TCARBOX	"cars\schoenerbus\autokasten.bmp"
TSHADOW "cars\schoenerbus\schatten.bmp"
SHADOWTABLE -120.0 120.0 142.5 -116.25 -5.0
COLL 	"cars\schoenerbus\rumpf.hul"
EnvRGB 	64 64 64
SFXENGINE	"cars\schoenerbus\benzin.wav"

;====================
; Stuff mainly for frontend display and car selectability
;====================

BestTime   	TRUE
Selectable 	TRUE
Statistics	TRUE
Class      	1 			; Engine type (0=Elec, 1=Glow, 2=Other)
Obtain     	0 			; Obtain method
Rating     	3 			; Skill level (rookie, amateur, ...)
TopEnd     	3734.968750 			; Actual top speed (mph) for frontend bars
Acc        	7.871933 			; Acceleration rating (empirical)
Weight     	7.000000 			; Scaled weight (for frontend bars)
Handling   	50.000000 			; Handling ability (empirical and totally subjective)
Trans      	0 			; Transmission type (calculate in game anyway...)
MaxRevs    	0.500000 			; Max Revs (for rev counter)

;====================
; Handling related stuff
;====================

SteerRate  	3.000000 			; Rate at which steer angle approaches value from input
SteerMod   	0.400000 			;
EngineRate 	4.500000 			; Rate at which Engine voltage approaches set value
TopSpeed   	44.666000 			; Car's theoretical top speed (not including friction...)
DownForceMod	2.000000 			; Down force modifier when car on floor
CoM        	0.000000 -10.000000 0.000000 		; Centre of mass relative to model centre
Weapon     	0.000000 -32.000000 64.000000 		; Weapon genration offset

;====================
; Car Body details
;====================

BODY {		; Start Body
ModelNum   	0 			; Model Number in above list
Offset     	0, 0, 0 		; Calculated in game
Mass       	7.000000
Inertia    	19500.000000 0.000000 0.000000
           	0.000000 45000.000000 0.000000
           	0.000000 0.000000 17000.000000
Gravity		2200 			; No longer used
Hardness   	0.000000
Resistance 	0.001000 			; Linear air esistance
AngRes     	0.001000 			; Angular air resistance
ResMod     	10.000000 			; Ang air resistnce scale when in air
Grip       	0.010000 			; Converts downforce to friction value
StaticFriction 0.800000
KineticFriction 0.400000
}     		; End Body

;====================
; Car Wheel details
;====================

WHEEL 0 { 	; Start Wheel
ModelNum 	1
Offset1  	-31.125000 0.000000 72.562500
Offset2  	0.000000 -0.000000 -0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	TRUE
SteerRatio  	-0.450000
EngineRatio 	100000.000000
Radius      	22.500000
Mass        	0.600000
Gravity     	2200.000000
MaxPos      	5.000000
SkidWidth   	22.500000
ToeIn       	0.000000
AxleFriction    	0.020000
Grip            	0.015000
StaticFriction  	1.750000
KineticFriction 	1.600000
}          	; End Wheel

WHEEL 1 { 	; Start Wheel
ModelNum 	2
Offset1  	31.125000 0.000000 72.562500
Offset2  	0.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	TRUE
SteerRatio  	-0.450000
EngineRatio 	100000.000000
Radius      	22.500000
Mass        	0.600000
Gravity     	2200.000000
MaxPos      	5.000000
SkidWidth   	22.600000
ToeIn       	0.000000
AxleFriction    	0.020000
Grip            	0.015000
StaticFriction  	1.750000
KineticFriction 	1.600000
}          	; End Wheel

WHEEL 2 { 	; Start Wheel
ModelNum 	1
Offset1  	-31.125000 0.000000 -72.562500
Offset2  	0.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	TRUE
SteerRatio  	0.200000
EngineRatio 	100000.000000
Radius      	22.500000
Mass        	0.600000
Gravity     	2200.000000
MaxPos      	5.000000
SkidWidth   	22.500000
ToeIn       	0.000000
AxleFriction    	0.050000
Grip            	0.015000
StaticFriction  	1.700000
KineticFriction 	1.600000
}          	; End Wheel

WHEEL 3 { 	; Start Wheel
ModelNum 	2
Offset1  	31.125000 0.000000 -72.562500
Offset2  	0.000000 0.000000 0.000000
IsPresent   	TRUE
IsPowered   	TRUE
IsTurnable  	TRUE
SteerRatio  	0.200000
EngineRatio 	100000.000000
Radius      	22.500000
Mass        	0.600000
Gravity     	2200.000000
MaxPos      	5.000000
SkidWidth   	22.500000
ToeIn       	0.000000
AxleFriction    	0.050000
Grip            	0.015000
StaticFriction  	1.700000
KineticFriction 	1.600000
}          	; End Wheel


;====================
; Car Spring details
;====================

SPRING 0 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	800.000000
Damping     	10.000000
Restitution 	-0.700000
}           	; End Spring

SPRING 1 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
Stiffness   	800.000000
Damping     	10.000000
Restitution 	-0.700000
}           	; End Spring

SPRING 2 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 -0.000000
Length      	0.000000
Stiffness   	1000.000000
Damping     	10.000000
Restitution 	-0.700000
}           	; End Spring

SPRING 3 { 	; Start Spring
ModelNum    	-1
Offset      	0.000000 0.000000 -0.000000
Length      	0.000000
Stiffness   	1000.000000
Damping     	10.000000
Restitution 	-0.700000
}           	; End Spring


;====================
; Car Pin details
;====================

PIN 0 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 1 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 2 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin

PIN 3 {    	; Start Pin
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End Pin


;====================
; Car axle details
;====================

AXLE 0 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 1 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 2 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle

AXLE 3 {   	; Start Axle
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Length      	0.000000
}           	; End axle


;====================
; Car spinner details
;====================

SPINNER {   	; Start spinner
ModelNum    	-1
Offset      	0.000000 0.000000 0.000000
Axis        	0.000000 1.000000 0.000000
AngVel      	1.000000
}           	; End Spinner


;====================
; Car Aerial details
;====================

AERIAL {    	; Start Aerial
SecModelNum 	17
TopModelNum 	18
Offset      	26.250000 -59.250000 -101.250000
Direction   	0.000000 -1.000000 0.000000
Length      	30.000000
Stiffness   	2000.000000
Damping     	5.500000
}           	; End Aerial


;====================
; Car AI details
;====================

AI {        	 ;Start AI
UnderThresh 	82.150002
UnderRange  	3162.633545
UnderFront	 	251.520004
UnderRear   	1125.232666
UnderMax    	0.271073
OverThresh  	1704.070923
OverRange   	1391.000000
OverMax     	0.746230
OverAccThresh  	113.930000
OverAccRange   	2574.388916
PickupBias     	16383
BlockBias      	16383
OvertakeBias   	16383
Suspension     	9830
Aggression     	0
}           	; End AI

;====================
; Camera
;====================
CAMATTACHED { ; Start Camera
HoodOffset 0.00 1750.00 250.00
HoodLook 0.25
RearOffset 0.00 100.00 -150.00
RearLook 0.02
} ; End Camera

}

N1C3BU5